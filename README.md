# deliver.media DMRN Parser

[![Build Status](https://jenkins.smps.io/buildStatus/icon?job=dmrn-parser/master)](https://jenkins.smps.io/job/dmrn-parser/job/master/)

This deliver.media DMRN parser works for both DMRN and ARN resource names. As deliver.media's DMRN are based on the 
same structure as the ARN, they are compatible.

See: <http://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html>

## Example use

```javascript
var dmrnParser = require("dmrn-parser");
var myAwsString = "dmrn:aws:s3:eu-central-1::deliver-media-static-media/uploads/file.jpg";

var dmrn = dmrnParser(myAwsString);

console.log(dmrn);
```

With ES6:

```javascript
import dmrnParser from 'dmrn-parser';
const myAwsString = 'dmrn:aws:s3:eu-central-1::deliver-media-static-media/uploads/file.jpg';

const dmrn = dmrnParser(myAwsString);

console.log(dmrn);
```

The response is an array consisting of:

```javascript
dmrn.arn           = 'dmrn';
dmrn.partition     = 'aws';
dmrn.service       = 's3';
dmrn.region        = 'eu-central-1';
dmrn.account       = '';
dmrn.resourcetype  = 'deliver-media-static-media';
dmrn.resource      = 'uploads/file.jpg';
```

## Installation

```bash
npm install git+ssh://git@bitbucket.org:smpsolutions/dmrn-parser.git
```

## Compilation

The module is written in ES6 in the `/src` folder and transpiled into JS using babel.

```bash
npm run compile
```

## Tests

Run tests with

```bash
npm run test
```
/**
 * Parser for deliver.media DMRN or ARN Resource Names
 * Based on gene1wood's https://gist.github.com/gene1wood/5299969edc4ef21d8efcfea52158dd40
 * @type {Array}
 */
export default function (dmrnString) {
    const elements = dmrnString.split(':');
    let result = {
        'arn': elements[0],
        'partition': elements[1],
        'service': elements[2],
        'region': elements[3],
        'account': elements[4]
    };

    if (elements.length === 7) {
        result['resourcetype'] = elements[5];
        result['resource'] = elements[6];
    } else if (elements[5].indexOf('/') < 0) {
        result['resource'] = elements[5];
        result['resourcetype'] = null
    } else {
        const splitPos = elements[5].indexOf('/');
        result['resourcetype'] = elements[5].substr(0, splitPos);
        result['resource'] = elements[5].substr(splitPos + 1);
    }

    return result;
};
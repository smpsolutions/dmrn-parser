// Require and instantiate jsdom for browser function testing
import assert from 'assert';
import 'babel-polyfill'
import dmrnParser from '../lib/dmrn-parser';

describe('dmrn-parser-es6', function () {
    before(function () {

    });
    describe('parseArn', function () {
        it('dmrn s3 storage parsing for file store shall work', function () {
            const p1 = dmrnParser('dmrn:aws:s3:eu-central-1::deliver-media-static-media/uploads/59272e2fe37e91c7843c591e/d1ba83f6-4063-4f29-9132-fccf3015a3a8_test.jpg');

            assert.equal(p1.arn, 'dmrn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 's3');
            assert.equal(p1.region, 'eu-central-1');
            assert.equal(p1.account, '');
            assert.equal(p1.resourcetype, 'deliver-media-static-media');
            assert.equal(p1.resource, 'uploads/59272e2fe37e91c7843c591e/d1ba83f6-4063-4f29-9132-fccf3015a3a8_test.jpg');
        });

        it('sample s3 arn shall work', function () {
            const p1 = dmrnParser('arn:aws:s3:::my_corporate_bucket/exampleobject.png');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 's3');
            assert.equal(p1.region, '');
            assert.equal(p1.account, '');
            assert.equal(p1.resourcetype, 'my_corporate_bucket');
            assert.equal(p1.resource, 'exampleobject.png');
        });
        it('sample s3 long path arn shall work', function () {
            const p1 = dmrnParser('arn:aws:s3:::my_corporate_bucket/path/path2/exampleobject.png');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 's3');
            assert.equal(p1.region, '');
            assert.equal(p1.account, '');
            assert.equal(p1.resourcetype, 'my_corporate_bucket');
            assert.equal(p1.resource, 'path/path2/exampleobject.png');
        });
        it('sample s3 bucket only path arn shall work', function () {
            const p1 = dmrnParser('arn:aws:s3:::my_corporate_bucket');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 's3');
            assert.equal(p1.region, '');
            assert.equal(p1.account, '');
            assert.equal(p1.resourcetype, null);
            assert.equal(p1.resource, 'my_corporate_bucket');
        });
        it('lambda arn shall work', function () {
            const p1 = dmrnParser('arn:aws:lambda:us-west-2:123456789012:function:ThumbNail');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 'lambda');
            assert.equal(p1.region, 'us-west-2');
            assert.equal(p1.account, '123456789012');
            assert.equal(p1.resourcetype, 'function');
            assert.equal(p1.resource, 'ThumbNail');
        });
        it('test complete arn shall work', function () {
            const p1 = dmrnParser('arn:partition:service:region:account-id:resourcetype:resource');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'partition');
            assert.equal(p1.service, 'service');
            assert.equal(p1.region, 'region');
            assert.equal(p1.account, 'account-id');
            assert.equal(p1.resourcetype, 'resourcetype');
            assert.equal(p1.resource, 'resource');
        });
        it('api gateway arn shall work', function () {
            const p1 = dmrnParser('arn:aws:execute-api:region:account-id:api-id/stage-name/HTTP-VERB/resource-path');

            assert.equal(p1.arn, 'arn');
            assert.equal(p1.partition, 'aws');
            assert.equal(p1.service, 'execute-api');
            assert.equal(p1.region, 'region');
            assert.equal(p1.account, 'account-id');
            assert.equal(p1.resourcetype, 'api-id');
            assert.equal(p1.resource, 'stage-name/HTTP-VERB/resource-path');
        });

    });
});
